'use strict';
const testTree = "(DIS:TE:0 (DIS:BA:0 (DIS:EL:0 (DIS:CONT:0 (TOP (S (NP (NN Nothing)) (VP (VBD was) (VP (VBG going) (S (VP (TO to) (VP (VB hold) (PRT (RP up)) (NP (NP (DT the)) (JJ long-delayed) (NN settlement) (PP (IN of) (NP (NP (NNP Britton)) (PP (IN vs.) (NP (NNP Thomasini))))))))))) (. .))) (TOP (FRAG (RB Not) (RB even) (NP (DT an) (NN earthquake)) (. .)))) (DIS:CONT:0 (DIS:span (DIS:TE:0 (TOP (S (PP (IN On) (NP (NP (DT the) (NN afternoon)) (PP (IN of) (NP (NNP Oct.) (CD 17))))) (, ,))) (DIS:EX:0 (PP (IN after) (NP (NP (NNS hours)) (PP (IN of) (S (VP (VBG haggling) (PP (IN with) (NP (CD five) (NNS insurance-claims) (NNS adjusters)))))))) (TOP (S (PP (PP (IN over) (S (VP (VBG settling) (NP (DT a) (NN toxic-waste) (NN suit)))))) (, ,))))) (TOP (S (NP (CD four) (NNS lawyers)) (VP (VBD had) (NP (DT an) (NN agreement)) (PP (IN in) (NP (NN hand)))) (. .)))) (DIS:CA:0 (DIS:span (DIS:TE:0 (CC But) (DIS:EN:0 (SBAR (IN as) (S (NP (NNP Judge) (NNP Thomas) (NNP M.) (NNP Jenkins)) (VP (VBD donned) (NP (PRP$ his) (NNS robes))))) (TOP (S (SBAR (SBAR (IN so) (S (NP (PRP he)) (VP (MD could) (VP (VB give) (NP (JJ final) (NN approval))))))) (, ,))))) (TOP (S (NP (DT the) (JJ major) (NN earthquake)) (VP (VBD struck) (, ,) (S (NP (PRP$ its) (NN epicenter)) (ADVP (RB not) (RB far) (PP (IN from) (NP (NP (PRP$ his) (NN courtroom)) (PP (IN in) (NP (NP (NNP Redwood) (NNP City)) (, ,) (NP (NNP Calif))))))))) (. .)))) (DIS:CA:0 (DIS:JO (TOP (S (S (NP (DT The) (NNS walls)) (VP (VBD shook))) (: ;))) (TOP (S (S (NP (DT the) (NN building)) (VP (VBD rocked))) (. .))) (DIS:span (DIS:CONT:0 (TOP (S (PP (IN For) (NP (DT a) (NN while))) (, ,) (NP (PRP it)) (VP (VBD looked) (SBAR (IN like) (S (NP (DT the) (NN deal))))))) (PRN (: --) (S (RB not) (VP (TO to) (VP (VB mention) (NP (NP (DT the) (NN courtroom)) (NP (PRP itself)))))) (: --))) (TOP (S (VP (VP (VBD was) (PP (IN on) (NP (NP (DT the) (NN verge)) (PP (IN of) (NP (NN collapse))))))) (. .))))) (DIS:EV:0 (DIS:AT:0 (DIS:AT:1 (DIS:JO (TOP (SINV (`` ``) (S (NP (DT The) (NN judge)) (VP (VP (VBD came) (ADVP (RB out))))))) (VP (CC and) (VP (VBD said) (, ,) (`` `)))) (TOP (SINV (S (S (ADVP (RB Quick)) (, ,) (VP (VB let) (S (NP (POS 's)) (VP (VB put) (NP (DT this)) (PP (IN on) (NP (DT the) (NN record)))))))) (, ,) ('' ') ('' '')))) (TOP (SINV (VP (VBZ says)) (NP (NP (NNP Sandy) (NNP Bettencourt)) (, ,) (NP (NP (DT the) (NN judge) (POS 's)) (NN court) (NN reporter))) (. .)))) (DIS:EL:0 (DIS:TE:0 (DIS:AT:1 (TOP (S (`` ``) (NP (PRP I)) (VP (VBD said) (, ,)))) (TOP (S (VP (`` `) (ADVP (RB NOW))) (. ?) ('' ')))) (DIS:JO (TOP (S (NP (PRP I)) (VP (VBD was) (VP (VBG shaking) (NP (DT the) (JJ whole) (NN time)))) (. .) ('' ''))) (TOP (S (NP (DT A) (JJ 10-gallon) (NN water) (NN cooler)) (VP (VBD had) (VP (VBN toppled) (PP (IN onto) (NP (DT the) (NN floor))) (, ,))))) (TOP (S (VP (VP (VBG soaking) (NP (DT the) (JJ red) (NN carpeting)))) (. .))) (TOP (S (S (NP (NNS Lights)) (VP (VBD flickered) (ADVP (RP on) (CC and) (RP off)))) (: ;))) (TOP (S (" +
	"S (NP (NN plaster)) (VP (VBD dropped) (PP (IN from) (NP (DT the) (NN ceiling))))) (, ,))) (S (NP (DT the) (NNS walls)) (ADVP (RB still)) (VP (VBD shook))) (TOP (S (CC and) (S (NP (DT an) (NN evacuation) (NN alarm)) (VP (VBD blared) (ADVP (RB outside)))) (. .))))) (DIS:JO (TOP (S (NP (DT The) (CD four) (NNS lawyers)) (VP (VBD climbed) (PP (IN out) (PP (IN from) (PP (IN under) (NP (DT a) (NN table)))))) (. .))) (DIS:AT:0 (TOP (SINV (`` ``) (S (VP (VB Let) (S (NP (POS 's)) (VP (VB close) (NP (DT the) (NN door)))))) (, ,) ('' ''))) (TOP (SINV (VP (VBD said)) (NP (DT the) (NN judge))))) (TOP (SINV (SBAR (IN as) (S (NP (PRP he)) (VP (VBD climbed) (PP (TO to) (NP (PRP$ his) (NN bench)))))) (. .)))))))))) (DIS:EV:0 (DIS:span (DIS:EL:0 (TOP (SINV (PP (IN At) (NP (NN stake))) (VP (VBD was)) (NP (NP (DT an) (ADJP ($ $) (CD 80,000)) (NN settlement))))) (DIS:EL:0 (VP (VBG involving) (SBAR (WHNP (WP who)) (S (VP (MD should) (VP (VB pay) (NP (NP (WP what) (NN share)) (PP (IN of) (NP (NP (NN cleanup) (NNS costs)) (PP (IN at) (NP (NP (DT the) (NN site)) (PP (IN of) (NP (DT a) (JJ former) (NN gas) (NN station))) (, ,))))))))))) (DIS:JO (SBAR (WHADVP (WRB where)) (S (NP (JJ underground) (NN fuel) (NNS tanks)) (VP (VBD had) (VP (VBN leaked))))) (TOP (SINV (NP (VP (CC and) (VP (VBN contaminated) (NP (DT the) (NN soil))))) (. .)))))) (TOP (S (CC And) (NP (DT the) (NNS lawyers)) (VP (VBD were) (ADJP (ADJP (RB just) (RB as) (JJ eager)) (PP (IN as) (NP (DT the) (NN judge))) (S (VP (TO to) (VP (VB wrap) (NP (PRP it)) (PRT (RP up))))))) (. .)))) (DIS:AT:0 (TOP (SINV (`` ``) (S (NP (PRP We)) (VP (VBD were) (ADVP (RB never)) (VP (VBG going) (S (VP (TO to) (VP (VB get) (S (NP (DT these) (NN insurance) (NNS companies)) (VP (TO to) (VP (VB agree) (ADVP (RB again))))))))))) (, ,) ('' ''))) (DIS:SU:0 (TOP (SINV (VP (VBZ says)) (NP (NP (NNP John) (NNP V.) (NNP Trump)) (, ,) (NP (NP (DT a) (NNP San) (NNP Francisco) (NN defense) (NN lawyer)) (PP (IN in) (NP (DT the) (NN case))))) (. .))) (DIS:CONT:0 (TOP (S (ADVP (RB Indeed)) (, ,) (NP (DT the) (NN insurance) (NNS adjusters)) (VP (VBD had) (ADVP (RB already)) (VP (VBN bolted) (PP (IN out) (PP (IN of) (NP (DT the) (NN courtroom)))))) (. .))) (DIS:EV:0 (TOP (S (NP (DT The) (NNS lawyers)) (VP (VBD went) (PP (TO to) (NP (VB work))) (ADVP (RB anyway)) (, ,)))) (DIS:AT:1 (S (ADVP (RB duly)) (VP noting)) (TOP (S (VP (SBAR (IN that) (S (NP (DT the) (NN proceeding)) (VP (VBD was) (VP (VBG taking) (NP (NN place)) (PP (IN during) (NP (DT a) (JJ major) (NN earthquake)))))))) (. .)))))))))) (DIS:EV:0 (TOP (S (ADVP (NP (CD Ten) (NNS minutes)) (RB later)) (, ,) (NP (PRP it)) (VP (VBD was) (VP (VBN done))) (. .))) (DIS:JO (DIS:AT:1 (TOP (S (PP (IN For) (NP (DT the) (NN record))) (, ,) (NP (NP (NNP Jeffrey) (NNP Kaufman)) (, ,) (NP (NP (DT an) (NN attorney)) (PP (IN for) (NP (NP (NN Fireman) (POS 's)) (NN Fund)))) (, ,)) (VP said))) (DIS:CONT:0 (SBAR (S (NP (PRP he)) (VP (VBD was) (`` ``) (VP rattled)))) (TOP (S (VP (VP (: --) (ADVP (DT both) (R" +
	"B literally) (CC and) (RB figuratively)))) (. .) ('' ''))))) (DIS:CONT:0 (DIS:AT:0 (DIS:AT:1 (TOP (SINV (`` ``) (S (NP (PRP$ My) (NN belief)) (VP (VBZ is) (ADVP (RB always)) (, ,))))) (DIS:COND:1 (SBAR (S (SBAR (IN if) (S (NP (PRP you)) (VP (VBP 've) (VP (VBD got) (NP (DT a) (NN settlement)))))) (, ,))) (TOP (SINV (S (S (NP (PRP you)) (VP (VBP read) (NP (PRP it)) (PP (IN into) (NP (DT the) (NN record)))))) (. .) ('' ''))))) (DIS:EL:0 (TOP (SINV (VP (VBZ says)) (NP (NP (NNP Judge) (NNP Jenkins)) (, ,)))) (TOP (SINV (NP (VP (ADVP (RB now)) (VBN known) (PP (IN in) (NP (PRP$ his) (NN courthouse))) (PP (IN as) (`` ``) (NP (NNP Shake) (NNP 'Em) (NNP Down) (NNP Jenkins))))) (. .) ('' ''))))) (TOP (S (NP (DT The) (NN insurance) (NNS adjusters)) (VP (VBP think) (ADVP (RB differently))) (. .)))) (DIS:EV:0 (DIS:AT:0 (DIS:AT:1 (TOP (SINV (`` ``) (S (NP (PRP I)) (VP (VBD did) (RB n't) (VP know))))) (TOP (SINV (S (SBAR (IN if) (S (NP (PRP it)) (VP (VBD was) (NP (NP (NNP World) (NNP War) (NNP III)) (CC or) (NP (WP what))))))) (, ,) ('' '')))) (TOP (SINV (VP (VBZ says)) (NP (NP (NNP Melanie) (NNP Carvain)) (PP (IN of) (NP (NP (NNP Morristown)) (, ,) (NP (NNP N.J))))) (. .)))) (TOP (S (`` ``) (S (VP (VBG Reading) (NP (DT the) (NN settlement)) (PP (IN into) (NP (DT the) (NN record))))) (VP (VBD was) (NP (NP (DT the) (JJ last) (NN thing)) (PP (IN on) (NP (PRP$ my) (NN mind))))) (. .) ('' '')))))))";


class Node {
	constructor(parent, str, level = 0){
		let children = [];
		let nodeText = null;
		let type = /\((\S*).*\)/.exec(str)[1];

		this._parent = parent;
		this._type = type;
		this._isDiscourse = this.type.slice(0,3) == 'DIS';
		this._level = level;
		this._isCollapsed = this.parent && this.parent.isDiscourse && !this.isDiscourse;

		//get child(ren) substring
		let rxGetSubstr = /\(\S+\s(\(\S.*\))\)/.exec(str);

		//if theres no substr we've reached root node
		if(rxGetSubstr){
			//at least 1 substr -> at least 1 child
			let subtreeStr = rxGetSubstr[1];
			let lvl = 0;
			for(let i = 0; i < subtreeStr.length; i++){
				if(subtreeStr[i] == '('){
					lvl++;
					let beg = i + 0;
					for(i = i + 1; i < subtreeStr.length; i++){
						if(subtreeStr[i] == '('){
							lvl++;
						}
						if(subtreeStr[i] == ')'){
							lvl--;
						}
						if(lvl === 0){
							//new child
							children.push(new Node(this, subtreeStr.slice(beg, i + 1), level + 1));
							break;
						}
					}
				}
			}
		} else {
			//might have text (a word/etc.)

			let txt = /\(\S*\s(\S*)\)/.exec(str);
			if(txt){
				//testSentence +=
				//	txt[1][0].match(/[a-zA-Z0-9`]/) &&
				//	testSentence[testSentence.length - 1] != '`' ?
				//	' ' + txt[1] :
				//	txt[1];
				nodeText = txt[1];
			}
		}

		this._children = children;
		this._text = nodeText;

		if(!this.parent){
			this.buildURI();
		}
	}

	/*
	 * buildURL creates a unique ID for the node
	 * which is also a map to the node in the tree
	 */
	buildURI(parentId = null, positionInSiblings = ''){
		this._id = `${parentId ? `${parentId}:` : ''}${positionInSiblings}`;

		this.children.forEach( (c, i) => c.buildURI(this.id, i) );
	}

	getNodeFromURI(paths){
		if(paths.length === 0 || (paths.length === 1 && paths[0] === '')){
			return this;
		}

		return this.children[paths.shift()].getNodeFromURI(paths);
	}

	//builds the locations of the nodes for sigmajs graphs
	buildSigmaXY(){
		const ySize = 10.0;
		const xSize = 10.0;
		this._y = this.level * ySize;

		if(this.children.length === 0 || this.isCollapsed){
			//some random size
			this._x = 0;
			return xSize;
		}

		let childrenWidths = this.children
		.map( child => child.buildSigmaXY() );

		let totalChildrenWidth = childrenWidths.reduce( (s, n) => s + n, 0);

		//center this thing over middle of children
		this._x = (1.0 * totalChildrenWidth / 2.0) - (1.0 * xSize / 2.0);

		//scoot each child tree next to left sibling tree
		this.children
		.forEach(
			(c, i) =>
			c.adjustX(
				childrenWidths
				.slice(0, i)
				.reduce( (s, n) => s + n, 0)
			)
		);

		return totalChildrenWidth;
	}

	adjustX(xShift){
		if(Number.isNaN(Number.parseFloat(xShift))){
			throw new Error(`Cannot have NaN xShift: ${JSON.stringify(xShift)}`);
		}

		this._x += xShift;

		this.children.forEach( c => c.adjustX(xShift) );
	}

	get parent(){
		return this._parent;
	}

	get children(){
		return this._children;
	}

	get id(){
		return this._id;
	}

	get type(){
		return this._type;
	}

	get isCollapsed(){
		return this._isCollapsed;
	}

	get isDiscourse(){
		return this._isDiscourse;
	}

	get hasRawText(){
		return this._text ? true : false;
	}

	get level(){
		return this._level;
	}

	get x(){
		return this._x;
	}

	get y(){
		return this._y;
	}

	get text(){
		if(this._text){
			return this._text;
		}

		return this.children
		.map( c => c.text )
		.reduce( (s, t) => s + (t[0].match(/[a-zA-Z0-9`]/) && s[s.length - 1] != '`' ? ' ' + t : t), '' );
	}

	get nodeString(){
		let ts = this.children.length == 0 ? this.text : '';
		let cs = this.children
		.map( c => c.nodeString )
		.reduce( (ss, s) => `${ss} ${s}`, '' )
		.trim();

		let s = `(${this.type} ${ts}${cs})`;

		return s;
	}

	set isCollapsed(tOrF){
		//if it's a leaf node, it isnt collapsible
		if(this.children.length == 0){
			this._isCollapsed = false;
		} else {
			this._isCollapsed = tOrF ? true : false;
		}
	}

}

class SigmaGraphBuilder {
	constructor(diffs){
		this._diffs = diffs ? diffs : [];
	}

	get diffs(){
		return this._diffs;
	}

	set diffs(diffs){
		this._diffs = diffs;
	}

	static buildNode(node, label, size, color){
		return {
			id: node.id,
			label: label,
			size: size,
			x: node.x,
			y: node.y,
			color: (color ? color : (node.isCollapsed ? 'grey' : ''))
		};
	}

	static buildEdge(sourceNode, targetNode, showDirection = false, size = 0.2){
		return {
			id: `${sourceNode.id}e${targetNode.id}`,
			source: sourceNode.id,
			target: targetNode.id,
			size: size,
			type: (showDirection ? 'arrow' : '')
		};
	}

	applyDiffs(nodes){
		let redColour = '#ec5148';
		this.diffs.forEach( d => {
			let n = nodes
			.find( n => n.id === d.id );

			if(!n){
				return;
			}

			switch(d.type){
				case 'different':
					n.color = 'yellow';
				break;
				case 'hereNotThere':
					n.color = 'green';
				break;
				default:
					break;
			}
		});
	}

	build(nodeTree, diffs = []){
		let nodes = [];
		let edges = [];

		const size = 6;

		//arrow functions let us keep `this` scoping
		//recursively parses a Node object tree into sigmaJS representation
		let parseTree = (node) => {
			//generate label text
			let label;
			if(node.hasRawText){
				label = `${node.type}\n${node.text}`;
				//} else if(node.isCollapsed && node.isDiscourse){
				//label = `${node.type}\n${node.text}`;
			} else {
				label = node.type;
			}

			//save sigmaJS node obj
			nodes.push(SigmaGraphBuilder.buildNode(node, label, size));

			//edge from parent to current node
			if(node.parent){
				edges.push(SigmaGraphBuilder.buildEdge(node.parent, node, false, 0.5));
			}

			//if curr node is collapsed, dont show any children
			if(node.isCollapsed){
				return;
			}

			//generate children nodes
			for(let child of node.children){
				parseTree(child);
			}

			//peer linking (nodes at same level that need arrows connecting eachother)
			let idArr = node.type.split(':');
			let iNucleus = Number.parseInt(idArr[idArr.length - 1]);
			//if the current node is a discourse node with a nucleus index,
			//we need to peer link children using the index as the child that the arrow points to.
			if(!Number.isNaN(iNucleus)){
				if(!node.children){
					console.error(`Invalid discourse node ${node.type} requires children`);
				} else if(iNucleus !== 0 && node.children.length > 2){
					console.warn(`Complex nucleus-satellite relation not implemented: parent ${node.type} has non-0 nucleus with ${node.children.length} children`);
				} else {
					//arrows from every non-nucleus child to nucleus child

					let rootChild = node.children[iNucleus];
					node.children
					.filter( c => c !== rootChild )
					.forEach( c => edges.push(SigmaGraphBuilder.buildEdge(c, rootChild, true)) );
				}
			}
		};

		if(nodeTree){
			nodeTree.buildSigmaXY();
			parseTree(nodeTree);
			this.applyDiffs(nodes);
		}

		return {
			"nodes": nodes,
			"edges": edges
		};
	}
}

class GraphObj {
	constructor(nodeTree, htmlContainerId, settings, name){
		if(!(typeof htmlContainerId === 'string' || htmlContainerId instanceof String)){
			throw new TypeError(`Must provide id of HTML container: ${htmlContainerId}`);
		}

		this._name = name;
		this._htmlId = htmlContainerId;
		this.settings = settings;
		this.nodeTree = nodeTree;

		this._graphBuilder = new SigmaGraphBuilder();

		this._sigmaObj = new sigma({
			graph: this.graphBuilder.build(this.nodeTree),
			container: this.htmlId,
			settings: this.settings,
			renderers: [
				{
				//make sure its the canvas renderer,
				//so we all get the same experience
				container: document.querySelector(`#${this.htmlId}`),
				type: 'canvas'
			}
			],
		});

		for(let e in this.eventHandlers){
			this.sigmaObj.bind(e, this.eventHandlers[e]);
		}

		if(this.nodeTree){
			SigmaHelper.MoveCamera(this.sigmaObj, this.nodeTree.id, 0.4625);
		}
	}

	get name(){
		return this._name;
	}

	get graphBuilder(){
		return this._graphBuilder;
	}

	set diff(diffs){
		this.graphBuilder.diffs = diffs;
		this.refresh();
	}

	get settings(){
		return this._settings;
	}

	set settings(s){
		//we have default settings
		this._settings = s ? s : {
			edgeColor: 'default',
			//black edges
			defaultEdgeColor: '#000',
			minArrowSize: 7,
			defaultNodeColor: 'blue',
			fontStyle: 'bold',
			autoRescale: false
		};

		if(this.sigmaObj){
			//update sigma's settings
			this.sigmaObj.settings(this.settings);
			this.sigmaObj.refresh();
		}
	}

	get nodeTree(){
		return this._nodeTree;
	}

	set nodeTree(nt){
		if(typeof nt === 'string' || nt instanceof String){
			try {
				this._nodeTree = new Node(null, nt);
			} catch(e){
				console.warn(`Invalid text tree: ${e}`);
				this._nodeTree = null;
			}
		} else if(!nt){
			this._nodeTree = null;
		} else if(nt instanceof Node){
			this._nodeTree = nt;
		} else {
			throw new TypeError(`nodeTree must be null, string, or Node: ${Node}`);
		}

		if(this.sigmaObj){
			this.sigmaObj.graph.clear();
			this.sigmaObj.graph.read(this.graphBuilder.build(this.nodeTree));
			this.sigmaObj.refresh();
		}
	}

	get htmlId(){
		return this._htmlId;
	}

	get sigmaObj(){
		return this._sigmaObj;
	}

	refresh(){
		let g = this.graphBuilder.build(this.nodeTree);
		this.sigmaObj.graph.clear().read(g);
		this.sigmaObj.refresh();
	}

	get eventHandlers(){
		let node;

		let goTo = (id) => {
			SigmaHelper.MoveCamera(this.sigmaObj, id);
		};

		let getNode = (id) => {
			node = this.nodeTree.getNodeFromURI(id.split(':'));
			return node;
		}

		let toggleCollapse = (node) => {
			node.isCollapsed = node.isCollapsed ? false : true;
			this.refresh();
		}

		let showNodeMenu = (node) => {
			let el = document.querySelector('#nodeEditor');

			document.querySelector('#nodeEditorTitle').innerHTML = `${this.name} | ${node.type}`;
			document.querySelector('#nodeEditorTreeText').value = node.nodeString;
			document.querySelector('#nodeEditorCurrNodeId').value = node.id;

			//ugh, have to use jQuery this time
			$('#nodeEditor').modal();
		}

		let nodeClickHandler = (e) => {
			let nodeObj = getNode(e.data.node.id);

			if(e.data.captor.shiftKey && !e.data.captor.altKey){
				showNodeMenu(nodeObj);
				console.log(`nodeEditor node id: ${document.querySelector('#nodeEditorCurrNodeId').value}`);
			}

			//goTo(nodeObj.id);
			if(!e.data.captor.shiftKey && !e.data.captor.altKey){
				toggleCollapse(nodeObj);
			}

		};

		return {
			clickNode: nodeClickHandler,
		};
	}
}

class NodeTreeCompare {
	constructor(tree1, tree2, isCollapsedAware){
		if(!(tree1 instanceof Node)){
			throw new TypeError(`tree1 must be of type Node: ${tree1}`);
		}

		if(!(tree2 instanceof Node)){
			throw new TypeError(`tree2 must be of type Node: ${tree2}`);
		}

		this._tree1 = tree1;
		this._tree2 = tree2;
		this._isCollapsedAware = isCollapsedAware ? true : false;
		this.generateHashes();
	}

	generateHashes(){
		this._tree1Hash = NodeTreeCompare.HashNodeTree(this.tree1, this.isCollapsedAware);
		this._tree2Hash = NodeTreeCompare.HashNodeTree(this.tree2, this.isCollapsedAware);
	}

	get equivalence(){
		//lets not cache yet
		this.generateHashes();
		/*
		   if(this._equivalence){
		   return this._equivalence;
		   }
		   */

		const hash1 = this.tree1Hash;
		const hash2 = this.tree2Hash;

		if(hash1.length != hash2.length){
			this._equivalence = false;
		} else {
			this._equivalence = hash1.every( (h1, i) => h1 === hash2[i] );
		}

		return this._equivalence;
	}

	get diff(){
		let diffList1 = [];
		let diffList2 = [];

		if(this.equivalence){
			console.log('are equiv');
			return [ diffList1, diffList2 ];
		}

		function newDiff(id, type){
			return {
				id: id,
				type: type
			}
		}

		let diffSubTrees = (sTree1, sTree2) => {
			//compare nodes
			if(sTree1.type !== sTree2.type || (sTree1.hasRawText && sTree2.hasRawText && sTree1.text !== sTree2.text)){
				diffList1.push(newDiff(sTree1.id, 'different'));
				diffList2.push(newDiff(sTree2.id, 'different'));

				return;
			}

			//compare children
			sTree1.children
			.forEach( (c, i) => {
				if(i >= sTree2.children.length){
					//if its can't exist in sTree2, its only in tree 1
					diffList1.push(newDiff(c.id, 'hereNotThere'));
				} else {
					//compare the children
					diffSubTrees(c, sTree2.children[i]);
				}
			});

			//children in sTree2 that cant be in sTree1 are just in tree 2
			sTree2.children
			.filter( (c, i) => i >= sTree1.children.length )
			.forEach( c => diffList2.push(newDiff(c.id, 'hereNotThere')) );
		}

		diffSubTrees(this.tree1, this.tree2);

		return [ diffList1, diffList2 ];
	}

	get tree1Hash(){
		return this._tree1Hash;
	}

	get tree2Hash(){
		return this._tree2Hash;
	}

	get tree1(){
		return this._tree1;
	}

	get tree2(){
		return this._tree2;
	}

	get isCollapsedAware(){
		return this._isCollapsedAware;
	}

	static HashNodeTree(tree, isCollapsedAware, isLocaleAware = true){
		let hashList = [];

		let hash;

		let hashNode = (node) => {
			//hash node then children
			hash = `${node.type}${node.hasRawText ? '|' + node.text : ''}`;
			if(isLocaleAware){
				hash += `|${node.id}`;
			}
			if(isCollapsedAware){
				hash += `|${node.isCollapsed}`;
			}

			hashList.push(hash);

			node.children.forEach( c => hashNode(c) );
		}

		hashNode(tree);

		return hashList;
	}
}

class NodeClipboard {
	static push(node){
		if(!NodeClipboard.Clipboard){
			NodeClipboard._Clipboard = [];
		}

		NodeClipboard.Clipboard.push(node);
	}

	static pop(node){
		if(!NodeClipboard.Clipboard){
			NodeClipboard._Clipboard = [];
		}

		return NodeClipboard.Clipboard.pop();
	}

	static get Clipboard(){
		return NodeClipboard._Clipboard;
	}
}

//feature-focused functions that aren't integral to a graph
let SigmaHelper = {
	MoveCamera: function(sigmaObj, nodeId, zoom = null){
		let node = sigmaObj.graph.nodes().find( n => n.id === nodeId );
		if(!node){
			return;
		}

		let camera = sigmaObj.camera;

		let toTransform = {
			x: node['read_cam0:x'],
			y: node['read_cam0:y'],
			ratio: zoom ? zoom : camera.ratio,
			angle: camera.angle
		};

		camera.goTo(toTransform);

		//a nice, zoomed-in ratio is 0.0625
		/*
		   let preTransform = {
x: camera.x,
y: camera.y,
ratio: ,
angle: camera.angle
}
camera.goTo(preTransform);
SigmaGraph.refresh();
*/

		/*
		   sigma.misc.animation.camera(
		   camera,
		   toTransform,
		   { duration: 300 }
		   );
		   */
	},
	SetCollapseAll: function(graphObj, toCollapse){
		if(toCollapse !== false && toCollapse !== true){
			console.error(`bad non-boolean toCollapse passed: ${toCollapse}`);
		}

		let setCollapse = (node) => {
			node.children.forEach( c => setCollapse(c) );
			node.isCollapsed = toCollapse;
		}

		setCollapse(graphObj.nodeTree);
		graphObj.refresh();
	}
}

function doTreeDiff(){}
